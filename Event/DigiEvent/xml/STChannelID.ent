<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<method
 name = "isTT"
 type = "bool"
 desc = "check if TT type"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.isTT(); 
  </code> 
</method>

<method
 name = "isIT"
 type = "bool"
 desc = "check if IT type"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.isIT();
  </code> 
</method>

<method
 name = "station"
 type = "unsigned int"
 desc = "short cut for station"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.station();
  </code> 
</method>

<method
 name = "layer"
 type = "unsigned int"
 desc = "shortcut for layer"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.layer();
  </code> 
</method>

<method
 name = "detRegion"
 type = "unsigned int"
 desc = "short cut for detRegion"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.detRegion();
  </code> 
</method>

<method
 name = "sector"
 type = "unsigned int"
 desc = "short cut for sector"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.sector();
  </code> 
</method>

<method
 name = "strip"
 type = "unsigned int"
 desc = "short cut for strip"
 const = "TRUE" >
  <code>
  return &channelIDFunction;.strip();
  </code> 
</method>

<method
   name = "sectorName"
   desc = "Print the unique sector name"
   type = "std::string"
   virtual = "FALSE"
   const = "TRUE">
     <code >
      return ( isTT() ? TTNames().UniqueSectorToString(&channelIDFunction;) :
                        ITNames().UniqueSectorToString(&channelIDFunction;) );
     </code>
</method>

<method
   name = "layerName"
   desc = "Print the unique layer name"
   type = "std::string"
   virtual = "FALSE"
   const = "TRUE">
     <code >
      return ( isTT() ? TTNames().UniqueLayerToString(&channelIDFunction;) :
                        ITNames().UniqueLayerToString(&channelIDFunction;) );
     </code>
</method>


<method
   name = "detRegionName"
   desc = "Print the unique det region name"
   type = "std::string"
   virtual = "FALSE"
   const = "TRUE">
     <code >
      return ( isTT() ? TTNames().UniqueRegionToString(&channelIDFunction;) :
                        ITNames().UniqueBoxToString(&channelIDFunction;) );
     </code>
</method>


<method
   name = "stationName"
   desc = "Print the station name"
   type = "std::string"
   virtual = "FALSE"
   const = "TRUE">
     <code >
      return ( isTT() ? TTNames().StationToString(&channelIDFunction;) :
                        ITNames().StationToString(&channelIDFunction;) );
     </code>
</method>
